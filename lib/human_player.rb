class HumanPlayer
  def initialize(name, mark=nil, board=nil)
    @name = name
    @mark = mark
    @board = board
  end

  attr_accessor :name, :mark, :board

  def get_move
    print "Please enter where to move: (column , row) :"
    move = gets.chomp

    column = move[1].to_i
    row = move[3].to_i

    [column, row]
  end

  def display(board)
    @board = board
    row_idx = 0
    while row_idx < 3
      column_idx = 0
      while column_idx < 3
        cell = board.grid[column_idx][row_idx]
        if cell.nil?
          print " - "
        else
          print " #{board.grid[column_idx][row_idx]} "
        end
        column_idx += 1
      end
      puts
      row_idx += 1
    end
    puts '-' * 25
    @board = board 
  end

end

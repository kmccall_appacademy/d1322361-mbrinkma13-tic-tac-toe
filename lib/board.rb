class Board
  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(position, mark)
    if empty?(position)
      grid[position[0]][position[1]] = mark
    else
      raise "Position #{position} is taken!"
    end
  end

  def empty?(position)
    if grid[position[0]][position[1]].nil?
      return true
    end
    false
  end

  def winner
    winning_combos = [[[0,0],[0,1],[0,2]], # top horizontal
                      [[1,0],[1,1],[1,2]], # mid horizontal
                      [[2,0],[2,1],[2,2]], # bottom horizontal
                      [[0,0],[1,1],[2,2]], # left diagonal
                      [[0,2],[1,1],[2,0]], # right diagonal
                      [[0,0],[1,0],[2,0]], # left vertical
                      [[0,1],[1,1],[2,1]], # mid vertical
                      [[0,2],[1,2],[2,2]]] # right vertical

    winning_combos.each do |combo_set|
      x_count = 0
      o_count = 0

      combo_set.each do |combo|
        if grid[combo[1]][combo[0]] == :X
          x_count += 1
        elsif grid[combo[1]][combo[0]] == :O
          o_count += 1
        end
      end

      if x_count == 3
        return :X
      elsif o_count == 3
        return :O
      end
    end

    nil
  end


  def over?
    if winner
      true
    elsif mark_count == 9
      "It is a tie!"
    else
      false
    end
  end

  def mark_count
    mark_count = 0
    grid.each do |row|
      row.each do |cell|
        if cell
          mark_count += 1
        end
      end
    end

    mark_count
  end

end

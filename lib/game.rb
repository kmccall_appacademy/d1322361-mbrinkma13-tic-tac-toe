require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
# require 'byebug'

class Game
  attr_accessor :board, :current_player, :player_one, :player_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    player_one.mark = :O
    player_two.mark = :X
    @current_player = player_one
  end

  def play
    play_turn until board.over?
  end

  def play_turn
    # get move from the current player
    move = current_player.get_move

    # perform the move
    if board.place_mark(move, current_player.mark)
      player_one.display(board)
      switch_players!
    else
      puts "Please enter a valid move: "
      current_player.get_move
    end

  end

  def switch_players!
    if @current_player == @player_one
      instance_variable_set(:@current_player, @player_two)
    elsif @current_player == @player_two
      instance_variable_set(:@current_player, @player_one)
    end
  end

end

if __FILE__ == $PROGRAM_NAME
# debugger
puts "Player 1: "
one_name = gets.chomp
puts "Player 2: "
two_name = gets.chomp

player_one = HumanPlayer.new(one_name)
player_two = ComputerPlayer.new(two_name)

game = Game.new(player_one, player_two)
player_two.display(game.board)
game.play

end

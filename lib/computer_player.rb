class ComputerPlayer
  def initialize(name, mark=nil, board=nil)
    @name = name
    @mark = mark
    @board = board
  end

  attr_accessor :name, :mark, :board

  def get_move
    # get array of empty cell coordinates (potential moves)
    open_moves = []
    column_n = 0
    board.grid.each do |column|
      row_n = 0
      column.each do |row|
        if row.nil?
          open_moves.push([column_n, row_n])
        end
        row_n += 1
      end
      column_n += 1
    end

    # If moving to one of the empty cells wins, return that move
    possible_board = board
    open_moves.each do |move|
      possible_board.place_mark(move, mark)
      if possible_board.winner == mark
        possible_board.grid[move[0]][move[1]] = nil
        return move
      end
      possible_board.grid[move[0]][move[1]] = nil
    end

    # else move to a random open cell
    random_move = open_moves.shuffle[0]
    random_move
  end


  def display(board)
    @board = board
    board.grid.each do |row|
      row.each do |cell|
        print cell.to_s + ' '
      end
      puts
    end
    @board = board
  end

end
